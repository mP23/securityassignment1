import java.io.*; 
import java.net.*; 
class TCPServer { 
    public static void main(String argv[]) throws Exception{
       String clientSentence;
       String capitalizedSentence;
       ServerSocket listen = new ServerSocket(4504);
       while(true) {
	  Socket conn = listen.accept(); 
	  //conn.setSoTimeout(20);
	  BufferedReader in = new BufferedReader(new
		   InputStreamReader(conn.getInputStream())); 
//	  DataOutputStream out = 
//	     new DataOutputStream(conn.getOutputStream());
	  PrintWriter out = 
	     new PrintWriter(conn.getOutputStream(),true);
	  clientSentence = in.readLine();
	  System.out.println("FROM CLIENT:" + clientSentence);
	  capitalizedSentence = clientSentence.toUpperCase();
	  out.println(capitalizedSentence);
	  conn.close();
      } 
   } 
} 

